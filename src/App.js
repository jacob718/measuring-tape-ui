import React, { useEffect, useState } from 'react';
import { getResourceTypeNames } from './services';
import Calculator from './components/calculator';
import { Select } from '@material-ui/core';
import Title from './components/title';
import './app.scss';
import Grid from '@material-ui/core/Grid';

function App() {
  const [resources, setResources] = useState([]);
  const [resource, setResource] = useState("Concrete Slab");

  useEffect(() => {
    getResourceTypeNames()
        .then(response => {
            setResources(response.names);
        });
  }, []);

  return (
    <div className="container">
      <Grid container direction="column" spacing={3}>
        <Grid item xs={12}>
          <Title direction="column" alignItems="center"/>
        </Grid>

        <Grid item xs={12}>
          <Select className="select"
              native
              variant="outlined"
              onChange={e => setResource(e.target.value)}
              inputProps={{
                name: 'age',
              }}
            >
              {resources.map(i => <option key={i} value={i}>{i}</option>)}
          </Select>
        </Grid>

        <Grid item xs={12}>
          <div className="calc">
            <Calculator resourceName={resource}/>
          </div>
        </Grid>
      </Grid>
      <div className="box">
      </div>
    </div>
    
  );
}

export default App;
