import React, { useEffect, useState } from 'react';
import { calculate, getResourceType } from '../services';
import Result from './result';
import TextField from '@material-ui/core/TextField';
import { Button } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import './calculator.scss';

const slugify = require('slugify');

const Calculator = (props) => {
    const { resourceName } = props;

    const [resource, setResource] = useState({});
    const [varNames, setVarNames] = useState([]);
    const [varVals, setVarVals] = useState([]);
    const [varMeas, setVarMeas] = useState([]);
    const [result, setResult] = useState({});
    const [slug, setSlug] = useState('concrete-slab');

    useEffect(() => {
        setSlug(slugify(resourceName, { lower: true }))

    }, [resourceName]);

    useEffect(() => {
        setSlug(slugify(resourceName, { lower: true }))

        getResourceType(slug)
        .then(response => {

        setVarNames(response.varNames);
        setResource(response);
        setVarMeas(response.varMeas);
        });

    }, [slug, varVals]);

    useEffect(() => {
        calculate(varNames, varVals, slug)
        .then(response => {
            setResult(response);
        });
    }, [resource]);

    const handleSubmit = (e) => {
        // Prevents page from resetting
        e.preventDefault();

        var tempVals = [];

        for(var i = 0; i < e.target.length - 1; i ++) {
            if(e.target[i].value) {
                tempVals.push(parseInt(e.target[i].value));
            }
        }

        setVarVals(tempVals)
    }

    const inputBox = [];

    for(var i = 0; i < varNames.length; i++) {
        inputBox.push(
        <div>
            <Grid container direction="column" alignItems="flex-end">
                <Grid item xs={11}>
                    <TextField
                    className="input"
                    type='number'
                    id="standard-basic"
                    label={varNames[i]}
                    required
                    />
                </Grid>
                <Grid item xs={3}>
                    {varMeas[i]}
                </Grid>
            </Grid>
        </div>);
    };

    return(
    <div>
        <div>
            <form noValidate autoComplete="off" onSubmit={handleSubmit} on>
                <Grid container direction="column" alignItems="center" spacing={3}>
                    <Grid  className="bluegray" container direction="row" justifyContent="space-around" 
                    item xs={12} sm={6}>
                        {inputBox.map(i => <div>{i}</div>)}
                    </Grid>

                    <Grid item xs={12}>
                        <Button className="dark" type="submit" variant="contained">Calculate</Button>
                    </Grid>

                    <Grid item xs={12}>
                        <Result result={result} resource={resource.shorthand}
                        varNames={varNames} varVals={varVals} varMeas={varMeas} ></Result>
                    </Grid>
                </Grid>
            </form>
        </div>

    </div>
    )
}

export default Calculator;