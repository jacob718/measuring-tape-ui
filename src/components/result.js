import React, { useEffect, useState } from 'react';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import CopyToClipboard from 'react-copy-to-clipboard';
import Button from '@material-ui/core/Button';

const Result = (props) => {

    const { result, resource,
            varNames, varVals, varMeas } = props;
    
    var resultString = <div>Please enter all fields</div>;

    if(result.result == 0) {
        result.result = '0';
    }
    
    if(result.result) {
        resultString = 
            <div>
                <h2>{result.result}</h2>
                <div>
                {result.resultMeas} needed for this {resource}
                </div>
            </div>;
    };

    return(
        <div>
            <h2>
                RESULT <t></t>

                <CopyToClipboard text={result.result}>
                    <Button variant="outlined" color="primary"><FileCopyIcon/></Button>
                
                </CopyToClipboard>
            </h2>
            <h2>
                {resultString}
            </h2>
        </div>
    )
}

export default Result;