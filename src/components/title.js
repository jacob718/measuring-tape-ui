import React, { useEffect, useState } from 'react';
import './title.scss';
import { Typography, Paper } from '@material-ui/core';

const Title = () => {

    return(
        <div className="header">
            <div className="text-box">
                <div className="heading-primary">

                    <Typography className="heading-primary-main" variant="h3">MEASURING</Typography>
                    <Typography className="heading-primary-sub" variant="h3">TAPE</Typography>

                </div>
            </div>
            
        </div>
        
    )
}

export default Title;