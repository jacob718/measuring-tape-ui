export default {
    App: {
        "*": {
            padding: "0",
            margin: "0"
        },
        fontFamily: "Roboto",
        fontWeight: '400',
        letterSpacing: '1999px',
    },
    h1: {
        fontWeight: '400',
        fontSize: '1.5em',
        textTransform: 'uppercase',
        letterSpacing: '25px',
        fontFamily: "Lobster",
        "&:hover": { 
            color: "blue"
        }
    },
};