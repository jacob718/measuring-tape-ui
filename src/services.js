export const calculate = async (varNames, varValues, type) => {
    const params = paramIt(varNames, varValues);

    return fetch(`${process.env.MEASURE_API_URI}/resourcetype/${type}/calc${params}`)
    .then(response =>response.json())
    ;
};

const paramIt = (varNames, varVals) => {
    var url = '?';

    const length = varNames.length;

    for(var i = 0; i < length; i ++) {
        url = url.concat(`${varNames[i]}=${varVals[i]}`)
        if(i != length - 1) {
            url = url.concat('&');
        }
    };

    return(url);
}

export const getResourceType = async (slug) => {
    return fetch(`${process.env.MEASURE_API_URI}/resourcetype/${slug}`)
    .then(response =>response.json())
    .then(response => response.data)
    ;
};

export const getResourceTypeNames = async () => {
    return fetch(`${process.env.MEASURE_API_URI}/resourcetype/names`)
    .then(response =>response.json())
    ;
}